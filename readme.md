# Enriched YouTube RSS feed

This project improves YouTube's RSS feed by adding the videos' durations to their titles. It is built using YouTube native RSS feeds and the [Invidious](https://github.com/iv-org/invidious) API, an alternative front-end to YT. The resulting feed follows the [JSON Feed 1.1 specification](https://jsonfeed.org/version/1.1).

Developped to be deployed on [Vercel](https://vercel.com/).

![Screenshot of the Inoreader interface showing Kurzgesagt's feed with durations in the videos' titles](./img/results.png)

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Enriched YouTube RSS feed](#enriched-youtube-rss-feed)
	- [Deployment / Usage](#deployment-usage)
	- [Development / local usage](#development-local-usage)
	- [Q&A](#qa)
		- [Why not YouTube only?](#why-not-youtube-only)
		- [Can I use Invidious only, without YouTube?](#can-i-use-invidious-only-without-youtube)
		- [Could you explain how the project works?](#could-you-explain-how-the-project-works)
	- [Contributing](#contributing)
	- [Acknowledgment](#acknowledgment)
	- [Licence](#licence)

<!-- /TOC -->

## Deployment / Usage

1. Create a Vercel account
2. Fork the current repository
3. Synchronize it to a new Vercel project
3. Add environment variables to your Vercel project  (`Settings` > `Environment Variables` or read the [Vercel documentation](https://vercel.com/docs/environment-variables)):
- `INVIDIOUS_URL`: URL of an instance of Invidious. Check out the [official list](https://instances.invidious.io/) for examples. Note: not all instances allow an API access, test it before deploying.
- `VL_URL`: URL of your Vercel project.
4. Deploy the project (read the [Vercel documentation](https://vercel.com/docs/introduction#deploy-an-existing-project))
5. Navigate to `https://<example>.vercel.app/channel/<channel_id>/`

You should see a splendid JSONFeed.

## Development / local usage
1. Clone the project
```
git clone https://gitlab.com/sila_point/enriched-rss
```
2. Install npm packages
```
npm install
```
3. Create a `.env` file with `VL_URL` and `INVIDIOUS_URL`
Example of `.env`:
```
VL_URL="https://<example.com>/"
INVIDIOUS_URL="https://<example.com>/"
```
4. Install and start Vercel (read the [Vercel documentation](https://vercel.com/cli))
```
vercel dev
```

## Q&A
### Why not YouTube only?

![Oh no.](./img/youtube_api_ptsd.png)

### Can I use Invidious only, without YouTube?
Yes!
If you want to use Invidious as a standalone RSS provider, set the `INVIDIOUS_ONLY` environment variable to `1` in your `.env` file and/or in your Vercel project.

### Could you explain how the project works?

[I thought you'd never ask](https://sila.li/blog/youtube-video-duration-rss-feed/).

## Contributing

This is a small, kinda rushed project, but pull requests are welcome.

## Acknowledgment

Thanks to Jacket from Inoreader for their help in debugging the JSON Feed without answering "RTFM".

## Licence
[MIT](./licence.txt)
