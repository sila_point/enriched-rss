import fetch from 'node-fetch'
let Parser = require('rss-parser')

let parser = new Parser({
  customFields: {
    item: ['media:group'], // this is needed to get the thumbnail
  }
})


const vl_url       = process.env.VL_URL
const inv_base_url = process.env.INVIDIOUS_URL
const yt_base_url  = "https://youtube.com/"

let main_url = yt_base_url
let main_channel_url = yt_base_url + "channel/"

/* Invidious RSS feeds match nearly exactly the ones provided by YouTube
 So we can simply swap the URLs to create a valid feed based only on Invidious
*/
let inv_only = process.env.INVIDIOUS_ONLY == 1
if(inv_only) {
  main_url = inv_base_url
  main_channel_url = inv_base_url + "channel/"
}
let main_channel_feed_url = main_url + "feeds/videos.xml?channel_id="

export default async (req, res) => {
  const channel_id = req.query.id

  /* 1) Getting the channel's RSS feed based on its id */
  let feed = await parser.parseURL(main_channel_feed_url + channel_id)

  /* 2) Getting the durations from an Invidious instance */
  let iv_res = await fetch(inv_base_url + "api/v1/channels/" + channel_id + "?&fields=latestVideos")

  /* 3) Creating an object with video's IDs as keys containing the durations */
  let durations = {}
  // if the Invidious instance is down, failing gracefully
  if(iv_res.status == 200) {
    let iv_data = await iv_res.json()
    for(let vid of iv_data.latestVideos) {
        durations[vid.videoId] = vid.lengthSeconds
    }
  }

  for(let vid of feed.items) {
    /* 4) Adding the durations to the title of relevant videos */
    let id = vid.id.replace("yt:video:", "")
    if(id in durations) {
      // converting seconds to HH:MM:SS
      // https://stackoverflow.com/a/25279340
      let duration = new Date(durations[id] * 1000).toISOString().substr(11, 8)
      vid.title = "[" + duration + "] " + vid.title
    }

    /* 5) Adding the thumbnail / description */
    vid.image = vid['media:group']['media:thumbnail'][0]['$'].url
    vid.content_text = vid['media:group']['media:description'][0]

    /* 7) Removing the mediagroup now that we've used the thumbnail / description */
    delete vid['media:group']

    /* 8) Various modifications to create a valid JSONfeed */
    // https://jsonfeed.org/version/1.1
    vid.url = vid.link
    delete vid.link

    vid.date_published = vid.isoDate.replace('Z', '') // sneaky conversion from ISO to RFC3339
    delete vid.isoDate
    delete vid.pubDate

    vid.authors = [{name: vid.author}]

    // Invidious only
    delete vid.content
    delete vid.contentSnippet
  }

  /* 9) Final touches for a beautiful JSONfeed */
  feed.version = "https://jsonfeed.org/version/1.1"

  // Inoreader uses the URL to recognize the feed and we don't want to end up
  // with the default one from YouTube
  feed.feed_url = vl_url + 'channel/' + channel_id
  // that said, we use the YT channel as home page

  feed.home_page_url = main_channel_url + channel_id

  delete feed.link
  // Invidious only
  delete feed.feedUrl

  return res.json(feed)
};
